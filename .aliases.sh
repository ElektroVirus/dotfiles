alias gs='git status'
alias ga='git add'
alias gc='git commit'
alias gd='git diff'

alias ll="ls -lA"
alias l="ls -l"
alias please="sudo !!"

alias autistigcc="gcc -ansi -pedantic -Wall -lm -g"
alias flatpakdiscord="flatpak run com.discordapp.Discord"

alias trans="redfox-rs"

# Dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME/'


#
# Try to figure out the best editor
# 
if type nvim >/dev/null 2>&1; then
    export EDITOR="$(which nvim)"
elif type vim >/dev/null 2>&1; then
    export EDITOR="$(which vim)"
elif type vi >/dev/null 2>&1; then
    export EDITOR="$(which vi)"
fi

alias vim=$EDITOR
alias vi=$EDITOR


temp() {
    directory=$(mktemp -d)
    cd $directory
    rm_cmd="rm -rf $directory"
    echo "Running '$rm_cmd' in 7 days"
    echo "$rm_cmd" | at "now + 7 days"
}

function sharefile {
    RHOST="saga"
    UUID=$(uuidgen)
    RPATH="/var/www/share/$UUID"

    ssh $RHOST "mkdir $RPATH"
    scp $1 $RHOST:$RPATH
    echo "https://meyer.dy.fi/file/$UUID/$(basename $1)"

    time="7 days"
    echo "Deleting file in $time"
    rm_cmd="rm -rf $RPATH"
    remote_command="echo \"$rm_cmd\" | at \"now + $time\""
    ssh $RHOST $remote_command
}
