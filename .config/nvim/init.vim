" https://rust-analyzer.github.io/manual.html#installation

" Traverse < and > with %
set mps+=<:>
:map <F4> :nohl<CR>

autocmd! BufNewFile,BufRead *.typ,*.fs set ft=plain

" https://stackoverflow.com/questions/5559029/quickly-switching-buffers-in-vim-normal-mode
map <leader>n :bnext<cr>
map <leader>p :bprevious<cr>
map <leader>d :bdelete<cr>

" set tabstop=4
" set softtabstop=0 noexpandtab
" set shiftwidth=4
" set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
" set nowrap
highlight ColorColumn ctermbg=9

lua vim.o.tabstop = 4
lua vim.o.softtabstop = 4
lua vim.o.shiftwidth = 4
" lua vim.o.tabstop = 8
" lua vim.o.softtabstop = 0
" lua vim.o.expandtab = true
" lua vim.o.shiftwidth = 4
" lua vim.o.smarttab = true

lua vim.nowrap = true
lua vim.noexpandtab = true

lua vim.wo.number = true
lua vim.wo.relativenumber = true
" lua vim.opt.colorcolumn = "81,120"



" :set encoding=utf-8
" → \\u2192
" 🠊 \\U0001F80A
" · \\u00b7
" Vim config
" :set listchars=space:\\u00b7,tab:\\U0001F80A\\u0020
" :set list
" Lua config
" eol = "\\u21b2"
" lua vim.opt.listchars = {
"             \ leadmultispace = "\\u00b7\\u00b7\\u00b7|",
"             \ space = "\\u00b7",
"             \ tab = "\\U0001F80A\\u0020",
"             \ }
" lua vim.opt.list = true
" Space:    
" Tab:	
"###############################################################################
"# Plugins
"###############################################################################

call plug#begin()

Plug 'iamcco/markdown-preview.nvim', { 
    \ 'do': 'cd app & yarn install'  
    \ }

Plug 'preservim/nerdtree'

" Plug 'Townk/vim-autoclose'
" Not compatible with CoC:
" https://vi.stackexchange.com/questions/41834/down-arrow-key-triggers-cocpumvisible-cocpumnext0

" Plug 'vim-syntastic/syntastic'
" https://github.com/vim-syntastic/syntastic
" Deprecated, replace with ALE

Plug 'neoclide/coc.nvim', {
    \ 'branch': 'release'
    \ }

Plug 'neovim/nvim-lspconfig'

" Plug 'lukas-reineke/indent-blankline.nvim'

" Fuzzy finder
" Plug 'junegunn/fzf'

Plug 'ojroques/nvim-osc52'

call plug#end()

"##############################
"# nvim-osc52
"# https://github.com/ojroques/nvim-osc52
"##############################

" https://vi.stackexchange.com/questions/281/how-can-i-find-out-what-leader-is-set-to-and-is-it-possible-to-remap-leader#282
" By default, leader is set to \
lua vim.keymap.set('n', '<leader>c', require('osc52').copy_operator, {expr = true})
lua vim.keymap.set('n', '<leader>cc', '<leader>c_', {remap = true})
lua vim.keymap.set('v', '<leader>c', require('osc52').copy_visual)

"##############################
"# NerdTree
"# https://github.com/preservim/nerdtree
"##############################

nmap <C-n> :NERDTreeToggle<CR>

" note that if you are using Plug mapping you should not use `noremap` mappings.
nmap <F5> <Plug>(lcn-menu)
" Or map each action separately
nmap <silent>K <Plug>(lcn-hover)
nmap <silent> gd <Plug>(lcn-definition)
nmap <silent> <F2> <Plug>(lcn-rename)

" Required for operations modifying multiple buffers like rename.
set hidden

"##############################
"# lukas-reineke/indent-blankline.nvim
"https://github.com/lukas-reineke/indent-blankline.nvim
"##############################

" lua vim.opt.list = true
" " lua vim.opt.listchars:append "space:⋅"
" " lua vim.opt.listchars:append "eol:↴"
" 
" lua require("indent_blankline").setup {
"             \    show_end_of_line = true,
"             \    space_char_blankline = " ",
"             \ }

"##############################
"# Language Server 
"##############################

"
" LanguageClient-neovim
" https://github.com/autozimu/LanguageClient-neovim
" Probably not needed if I use CoC?
"
" let g:LanguageClient_serverCommands = {
"     \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
"     \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
"     \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
"     \ 'python': ['/usr/local/bin/pyls'],
"     \ 'ruby': ['~/.rbenv/shims/solargraph', 'stdio'],
"     \ }
" 
" " note that if you are using Plug mapping you should not use `noremap` mappings.
" nmap <F5> <Plug>(lcn-menu)
" " Or map each action separately
" nmap <silent>K <Plug>(lcn-hover)
" nmap <silent> gd <Plug>(lcn-definition)
" nmap <silent> <F2> <Plug>(lcn-rename)


"
" Conquer Of Completion
" https://github.com/neoclide/coc.nvim
"
:lua require("coc")
" Note: for code actions, use 
" - coc-codeaction-cursor
" - coc-codeaction-selected
" These can be installed by :CocInstall


"
" You Complete Me
" https://github.com/ycm-core/YouCompleteMe#installation
"


"
" Asynchronous Lint Engine (ALE)
" https://github.com/dense-analysis/ale
"
" let g:ale_linters = {'rust': ['analyzer']}


"
" nvim-lspconfig
" Do I need this?
" https://github.com/neovim/nvim-lspconfig
"
" lua require'lspconfig'.rust_analyzer.setup({})
" lua require("nvim-lspconfig")
" :lua require'lspconfig'.pyright.setup{}

