#!/bin/bash
set -euo pipefail

# Set to 'JWST' for Hubble Deep Field -image
TYPE=$(hostname)

if [ "$TYPE" == "JWST" ]; then
    lockscreen_image_path='$(xdg-user-dir HOME)/Pictures/BG/temp.jpg'
    /home/elias/Programming/Python/BackgroundCrop/venv/bin/python /home/elias/Programming/Python/BackgroundCrop/main.py
elif [ "$TYPE" == "NEBULA_HELIX" ]; then
    lockscreen_image_path="$(xdg-user-dir HOME)/.config/bg/nebula_helix.jpg"
elif [ "$TYPE" == "tellervo" ]; then
    export lockscreen_image_path="$(xdg-user-dir HOME)/.config/bg/aurora-over-iceland.png"
    wlogout
    exit
elif [ "$TYPE" == "ilmarinen" ]; then
    export lockscreen_image_path="$(xdg-user-dir HOME)/.config/bg/sunset_mountain_lake.jpg"
    wlogout
    exit
elif [ "$TYPE" == "tapio" ]; then
    export lockscreen_image_path="$(xdg-user-dir HOME)/.config/bg/nebula_gradient.jpg"
    wlogout
    exit
else
    lockscreen_image_path="$(xdg-user-dir HOME)/.config/bg/stars_and_nebula.jpg"
fi

swaylock -f -e -c 000000 -i $lockscreen_image_path
