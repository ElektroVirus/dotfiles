#!/bin/bash
set -euo pipefail

SCREENSHOT_DIR="$(xdg-user-dir PICTURES)/Screenshots"

if [ ! -d $SCREENSHOT_DIR ]; then
  mkdir -p $SCREENSHOT_DIR;
fi

filename=$(date +'screenshot_%Y-%m-%d-%H%M%S.png')
filepath=$SCREENSHOT_DIR/$filename
grim $filepath
wl-copy < $filepath
