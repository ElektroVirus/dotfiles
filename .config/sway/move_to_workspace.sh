#!/bin/sh
set -euo pipefail

display=$(~/.config/sway/get_active_display.sh)

swaymsg move container to workspace ${display}${1}
swaymsg workspace ${display}${1}
