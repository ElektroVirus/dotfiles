#!/bin/bash
set -euo pipefail

OUTPUT_DIR="$(xdg-user-dir VIDEOS)/wf-recorder/"

if [ ! -d $OUTPUT_DIR ]; then
  mkdir -p $OUTPUT_DIR;
fi

SUFFIX="mp4"
filename_tmp="$(date +'rec_%Y-%m-%d-%H%M%S_tmp').$SUFFIX"
filename="$(date +'rec_%Y-%m-%d-%H%M%S').$SUFFIX"
filepath_tmp=$OUTPUT_DIR/$filename_tmp
filepath=$OUTPUT_DIR/$filename

wf-recorder -g "$(slurp)" -f $filepath_tmp
ffmpeg -i $filepath_tmp $filepath
rm $filepath_tmp
