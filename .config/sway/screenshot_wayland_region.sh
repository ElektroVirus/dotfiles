#!/bin/bash
set -euo pipefail

SCREENSHOT_DIR="$(xdg-user-dir PICTURES)/Screenshots"

if [ ! -d $SCREENSHOT_DIR ]; then
  mkdir -p $SCREENSHOT_DIR;
fi

filename="last_screenshot.png"
filepath=$(xdg-user-dir PICTURES)/Screenshots/$filename
slurp | grim -g - $filepath
wl-copy < $filepath
