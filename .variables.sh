
################################################################################
# Environmental variables
################################################################################

##############################
# PATH
##############################

export PATH=$PATH:'/var/lib/flatpak/exports/share'
export PATH=$PATH:$HOME'/.local/share/flatpak/exports/share'
export PATH=$PATH:$HOME'/.cargo/bin'
export PATH=$PATH:$HOME'/scripts'
export PATH=$PATH:$HOME'/.bin'
export PATH=$PATH:$HOME'/.local/bin'

# arm-none-eabi-gcc
PATH_CANDIDATE="${HOME}/Software/ARM/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi/bin"
if [ -d ${PATH_CANDIDATE} ] ; then
    PATH=${PATH}:${PATH_CANDIDATE}
fi

# Golang
PATH_CANDIDATE="${HOME}/go/bin"
if [ -d ${PATH_CANDIDATE} ] ; then
    PATH=${PATH}:${PATH_CANDIDATE}
fi

##############################
# Wayland
##############################

export MOZ_ENABLE_WAYLAND=1
export QT_QPA_PLATFORM=wayland
# For screen sharing on Wayland (Sway)
# export XDG_CURRENT_DESKTOP=sway # Not apparently needed since sharing screen works now?

##############################
# Cargo
##############################

# Source cargo env
PATH_CANDIDATE="$HOME/.cargo/env"
if [ -d ${PATH_CANDIDATE} ] ; then
	source $PATH_CANDIDATE
fi
